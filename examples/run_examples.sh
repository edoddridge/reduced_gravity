cd ../


# f_plane.f90 - boring test with flat interface on f-plane
sed '40s/.*/    parameter(nx=100,ny=100)/' reduced_gravity.f90 > f_plane.f90
sed '43s/.*/    parameter(layers = 1)/' f_plane.f90 > examples/f_plane/f_plane.f90

rm f_plane.f90


# beta_plane_bump - test with a bump on a beta-plane
sed '40s/.*/    parameter(nx=100,ny=100)/' reduced_gravity.f90 > beta_plane_bump.f90
sed '43s/.*/    parameter(layers = 1)/' beta_plane_bump.f90 > examples/beta_plane_bump/beta_plane_bump.f90

rm beta_plane_bump.f90




# beta_plane_gyre - twin-gyre simulation on a beta-plane
sed '40s/.*/    parameter(nx=100,ny=200)/' reduced_gravity.f90 > beta_plane_gyre.f90
sed '43s/.*/    parameter(layers = 1)/' beta_plane_gyre.f90 > examples/beta_plane_gyre/beta_plane_gyre.f90

rm beta_plane_gyre.f90


# run f_plane example
cd examples/f_plane
rm -r output
rm -r renders
rm run_finished.txt
gfortran f_plane.f90 -o f_plane.out -Ofast
mkdir output
mkdir renders
./f_plane.out
cd ../


# run beta_plane_bump example
cd beta_plane_bump
rm -r output
rm -r renders
rm run_finished.txt
gfortran beta_plane_bump.f90 -o beta_plane_bump.out -Ofast
mkdir output
mkdir renders
./beta_plane_bump.out
cd ../


# run beta_plane_gyre example
# no slip
cd beta_plane_gyre
gfortran beta_plane_gyre.f90 -o beta_plane_gyre.out -Ofast

cd no_slip
rm -r output
rm -r renders
rm run_finished.txt
mkdir output
mkdir renders
../beta_plane_gyre.out


cd ../free_slip
rm -r output
rm -r renders
rm run_finished.txt
mkdir output
mkdir renders
../beta_plane_gyre.out
cd ../../