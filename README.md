# README #

NOTE: This model has been superseded by [Aronnax](https://github.com/edoddridge/aronnax), which includes a much friendlier Python wrapper, and the option of including a pressure solver.

A reduced gravity model with n + 1/2 layers on a cartesian grid.



## Examples ##

See the examples [here](http://nbviewer.jupyter.org/urls/bitbucket.org/edoddridge/reduced_gravity/raw/071b75f852f79910fb60148db3cd030b61404b36/examples/examples.ipynb).