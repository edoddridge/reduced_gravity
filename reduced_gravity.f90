!> @author
!> Ed Doddridge
!
!> Reduced gravity model with n +1/2 layers
!!
!
!>       
!>     @mainpage Documentation for reduced_gravity.f90
!>
!>     @section Overview
!>     This model is a reduced gravity model on an Arakawa C-grid with n + 1/2
!>     layers. 
!>
!>
!>            
!>    @section Grid
!>
!>    /\ ------------
!>    |  |          |
!>    |  |          |
!>    dy U    H     |
!>    |  |          |
!>    |  |          |
!>    \/ Z----V------
!>        <---dx---->
!>
!>    H: tracer point - thickness, Bernoulli potential
!>    U: velocity point - u and v
!>    Z: vorticity point - zeta
!>

program reduced_gravity
!     
!
    implicit none

!> number of x and y grid points
    integer nx,ny !< number of x and y grid points
    integer layers !< number of active layers in the model
    parameter(nx=100,ny=180)

!   number of active layers
    parameter(layers = 2)

!     layer thickness (h), and velocity components (u,v)
    double precision h(0:nx,0:ny,layers), dhdt(0:nx,0:ny,layers), u(nx,0:ny,layers),v(0:nx,ny,layers), &
        dudt(nx,0:ny,layers),dvdt(0:nx,ny,layers) 
    double precision dhdtold(0:nx,0:ny,layers), dudtold(nx,0:ny,layers),dvdtold(0:nx,ny,layers)         
    double precision dhdtveryold(0:nx,0:ny,layers), dudtveryold(nx,0:ny,layers),dvdtveryold(0:nx,ny,layers) 
    double precision hnew(0:nx,0:ny,layers), unew(nx,0:ny,layers),vnew(0:nx,ny,layers) 
!    Arrays for initialisation
    double precision hhalf(0:nx, 0:ny,layers), uhalf(nx,0:ny,layers), vhalf(0:nx,ny,layers)
!    for saving average fields
    double precision hav(0:nx,0:ny,layers),h2av(0:nx,0:ny,layers), uav(nx,0:ny,layers), vav(0:nx,ny,layers)
!
!     Bernoulli potential and relative vorticity 
    double precision b(nx,ny,layers),zeta(nx,ny,layers)

!    Grid
    double precision dx, dy
    double precision x_u(nx), y_u(0:ny)
    double precision x_v(0:nx), y_v(ny)
    double precision wetmask(0:nx,0:ny)
    double precision hfacW(nx,0:ny)
    double precision hfacE(nx,0:ny)
    double precision hfacN(0:nx,ny)
    double precision hfacS(0:nx,ny)
!    Coriolis parameter at u and v grid-points respectively
    double precision fu(nx,0:ny),fv(0:nx,ny)
    character(30) fUfile, fVfile
    character(30) wetMaskFile

!   Numerics
    double precision pi,au,ah(layers),ar,dt
    double precision slip
    double precision hmin
    integer nTimeSteps
    double precision dumpFreq, avFreq
    integer counter
    integer nwrite, avwrite
    double precision zeros(layers)

!   model
    double precision hmean(layers)

!   loop variables
    integer i,j,k,n

!   character variable for numbering the outputs
    character(10) num

!   Physics
    double precision gr(layers), rho0


!     Wind
    double precision wind_x(nx, 0:ny),wind_y(0:nx, ny)
    double precision base_wind_x(nx, 0:ny),base_wind_y(0:nx, ny)
    logical :: UseSinusoidWind
    logical :: UseStochWind
    logical :: DumpWind
    double precision :: wind_alpha, wind_beta, wind_period, wind_t_offset
    integer :: n_stoch_wind
    double precision :: stoch_wind_mag


!   Sponge
    double precision spongeHTimeScale(0:nx,0:ny,layers)
    double precision spongeUTimeScale(nx,0:ny,layers)
    double precision spongeVTimeScale(0:nx,ny,layers)
    double precision spongeH(0:nx,0:ny,layers)
    double precision spongeU(nx,0:ny,layers)
    double precision spongeV(0:nx,ny,layers)
    character(30) spongeHTimeScaleFile
    character(30) spongeUTimeScaleFile
    character(30) spongeVTimeScaleFile
    character(30) spongeHfile
    character(30) spongeUfile
    character(30) spongeVfile

!   input files
    character(30) initUfile,initVfile,initHfile
    character(30) zonalWindFile,meridionalWindFile

    NAMELIST /NUMERICS/ au,ah,ar,dt,slip,nTimeSteps,dumpFreq,avFreq,hmin

    NAMELIST /MODEL/ hmean

    NAMELIST /SPONGE/ spongeHTimeScaleFile,spongeUTimeScaleFile,spongeVTimeScaleFile, &
    spongeHfile,spongeUfile,spongeVfile
    
    NAMELIST /PHYSICS/ gr, rho0
    
    NAMELIST /GRID/ dx, dy, fUfile, fVfile, wetMaskFile
    
    NAMELIST /INITIAL_CONDITONS/ initUfile,initVfile,initHfile

    NAMELIST /EXTERNAL_FORCING/ zonalWindFile,meridionalWindFile, &
        UseSinusoidWind, UseStochWind, wind_alpha, wind_beta, &
        wind_period, wind_t_offset, DumpWind


    open(unit=8,file="parameters.in", status='OLD', recl=80)
    read(unit=8,nml=NUMERICS)
    read(unit=8,nml=MODEL)
    read(unit=8,nml=SPONGE)
    read(8,nml=PHYSICS)
    read(8,nml=GRID)
    read(8,nml=INITIAL_CONDITONS)
    read(8,nml=EXTERNAL_FORCING)
    close (unit = 8)


    nwrite = int(dumpFreq/dt)
    avwrite = int(avFreq/dt)

!    Pi, the constant
    pi=3.14159
!   Zero vector - for internal use only
    zeros = 0d0

! check that time dependent forcing flags have been set correctly
    if (UseSinusoidWind .and. UseStochWind)  then
        
        ! write a file saying so
        OPEN(UNIT=99, FILE='errors.txt', ACTION="write", STATUS="replace", &
            FORM="formatted")
        write(99,*) "Can't have both stochastic and sinusoidally varying wind forcings. Choose one."
        CLOSE(UNIT=99)

        ! print it on the screen
        print *, "Can't have both stochastic and sinusoidally varying wind forcings. Choose one."
        ! Stop the code
        STOP
    endif

!   Read in arrays from the input files
    call read_input_fileU(initUfile,u,0.d0,nx,ny,layers)
    call read_input_fileV(initVfile,v,0.d0,nx,ny,layers)
    call read_input_fileH(initHfile,h,hmean,nx,ny,layers)

    call read_input_fileU(fUfile,fu,0.d0,nx,ny,1)
    call read_input_fileV(fVfile,fv,0.d0,nx,ny,1)

    call read_input_fileU(zonalWindFile,base_wind_x,0.d0,nx,ny,1)
    call read_input_fileV(meridionalWindFile,base_wind_y,0.d0,nx,ny,1)

    call read_input_fileH(spongeHTimeScaleFile,spongeHTimeScale, zeros,nx,ny,layers)
    call read_input_fileH(spongeHfile,spongeH,hmean,nx,ny,layers)
    call read_input_fileU(spongeUTimeScaleFile,spongeUTimeScale,0.d0,nx,ny,layers)
    call read_input_fileU(spongeUfile,spongeU,0.d0,nx,ny,layers)
    call read_input_fileV(spongeVTimeScaleFile,spongeVTimeScale,0.d0,nx,ny,layers)
    call read_input_fileV(spongeVfile,spongeV,0.d0,nx,ny,layers)
    call read_input_fileH_2D(wetMaskFile,wetmask,0.d0,nx,ny)


! Initialise the average fields
    hav = 0.0
    uav = 0.0
    vav = 0.0

    if (wetMaskFile.eq.'') then
    ! 1 means ocean, 0 means land
        wetmask = 1d0 
        wetmask(0,:) = 0d0
        wetmask(:,0) = 0d0
        wetmask(nx,:) = 0d0
        wetmask(:,ny) = 0d0
    endif

    call calc_boundary_masks(wetmask,hfacW,hfacE,hfacS,hfacN,nx,ny)
     


!   If the winds are static, then set wind_ = base_wind_
    if (.not. UseSinusoidWind .and. .not. UseStochWind)  then
        wind_x = base_wind_x
        wind_y = base_wind_y
    endif


! initialise random numbers for stochastic winds
    if (UseStochWind) then
    ! this ensures a different series of random 
    ! numbers each time the model is run
        call ranseed()
    ! how many timesteps between updating the perturbed wind field.
        n_stoch_wind = int(wind_period/dt)
    endif




!    Do two initial time steps with Runge-Kutta second-order
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!    Initialisation of the model STARTS HERE            !!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!--------------------------- negative 2 time step -----------------------------
!    Code to work out dhdtveryold, dudtveryold and dvdtveryold


!    calculate Bernoulli potential
    call evaluate_b(b,h,u,v,nx,ny,layers,gr)

!    calculate relative vorticity!
    call evaluate_zeta(zeta,u,v,nx,ny,layers,dx,dy)

!     Calculate dhdt, dudt, dvdt at current time step
    call evaluate_dhdt(dhdtveryold, h,u,v,ah,dx,dy,nx,ny,layers,&
        spongeHTimeScale,spongeH,wetmask)

    call evaluate_dudt(dudtveryold, h,u,v,b,zeta,wind_x,fu, au,ar,&
        slip,dx,dy,hfacN,hfacS,nx,ny,layers,rho0,&
        spongeUTimeScale,spongeU)

    call evaluate_dvdt(dvdtveryold, h,u,v,b,zeta,wind_y,fv, &
        au,ar,slip,dx,dy,hfacW,hfacE,nx,ny,layers,rho0,spongeVTimeScale,spongeV)

!    Calculate the values at half the time interval with Forward Euler
    hhalf = h+0.5d0*dt*dhdtveryold
    uhalf = u+0.5d0*dt*dudtveryold
    vhalf = v+0.5d0*dt*dvdtveryold


!    calculate Bernoulli potential
    call evaluate_b(b,hhalf,uhalf,vhalf,nx,ny,layers,gr)

!    calculate relative vorticity
    call evaluate_zeta(zeta,uhalf,vhalf,nx,ny,layers,dx,dy)

!    Now calculate d/dt of u,v,h and store as dhdtveryold, dudtveryold and dvdtveryold
    call evaluate_dhdt(dhdtveryold, hhalf,uhalf,vhalf,ah,dx,dy,nx,ny,layers, spongeHTimeScale,spongeH,wetmask)

    call evaluate_dudt(dudtveryold, hhalf,uhalf,vhalf,b,zeta, &
        wind_x,fu, au,ar,slip,dx,dy,hfacN,hfacS,nx,ny,layers,rho0, &
        spongeUTimeScale,spongeU)

    call evaluate_dvdt(dvdtveryold, hhalf,uhalf,vhalf,b,zeta, &
        wind_y,fv, au,ar,slip ,dx,dy,hfacW,hfacE,nx,ny,layers,rho0,& 
        spongeVTimeScale,spongeV)
!    These are the values to be stored in the 'veryold' variables ready to start 
!    the proper model run.

!    Calculate h,u,v with these tendencies
    h = h + dt*dhdtveryold
    u = u + dt*dudtveryold
    v = v + dt*dvdtveryold

!    Apply the boundary conditions
!    Enforce no normal flow boundary condition
!    and no flow in dry cells.
!    no/free-slip is done inside dudt and dvdt subroutines.
!    hfacW and hfacS are zero where the transition between
!    wet and dry cells occurs. wetmask is 1 in wet cells,
!    and zero in dry cells.
    do k = 1,layers
        u(:,:,k) = u(:,:,k)*hfacW*wetmask(1:nx,:)
        v(:,:,k) = v(:,:,k)*hfacS*wetmask(:,1:ny)
    enddo



!--------------------------- negative 1 time step -----------------------------
!    Code to work out dhdtold, dudtold and dvdtold
!    calculate Bernoulli potential
    call evaluate_b(b,h,u,v,nx,ny,layers,gr)
!
!    calculate relative vorticity!
    call evaluate_zeta(zeta,u,v,nx,ny,layers,dx,dy)

!     Calculate dhdt, dudt, dvdt at current time step
    call evaluate_dhdt(dhdtold, h,u,v,ah,dx,dy,nx,ny,layers, spongeHTimeScale,spongeH,wetmask)

    call evaluate_dudt(dudtold, h,u,v,b,zeta,wind_x,fu, au,ar,slip,dx,dy,hfacN,hfacS,nx,ny,layers,rho0,spongeUTimeScale,spongeU)

    !if the wind is changed to be meridional this code will need changing

    call evaluate_dvdt(dvdtold, h,u,v,b,zeta,wind_y,fv, au,ar,slip, &
        dx,dy,hfacW,hfacE,nx,ny,layers,rho0,spongeVTimeScale,spongeV)

!    Calculate the values at half the time interval with Forward Euler
    hhalf = h+0.5d0*dt*dhdtold
    uhalf = u+0.5d0*dt*dudtold
    vhalf = v+0.5d0*dt*dvdtold


!    calculate Bernoulli potential
    call evaluate_b(b,hhalf,uhalf,vhalf,nx,ny,layers,gr)
!
!    calculate relative vorticity
    call evaluate_zeta(zeta,uhalf,vhalf,nx,ny,layers,dx,dy)

!    Now calculate d/dt of u,v,h and store as dhdtold, dudtold and dvdtold
    call evaluate_dhdt(dhdtold, hhalf,uhalf,vhalf,ah,dx,dy,nx,ny,layers, spongeHTimeScale,spongeH,wetmask)
    call evaluate_dudt(dudtold, hhalf,uhalf,vhalf,b,zeta,wind_x,&
        fu, au,ar,slip,dx,dy,hfacN,hfacS,nx,ny,layers,rho0,&
        spongeUTimeScale,spongeU)
    !if the wind is changed to be meridional this code will need changing
    call evaluate_dvdt(dvdtold, hhalf,uhalf,vhalf,b,zeta,wind_y,&
        fv, au,ar,slip, dx,dy,hfacW,hfacE,nx,ny,layers,rho0,& 
        spongeVTimeScale,spongeV)
!    These are the values to be stored in the 'old' variables ready to start 
!    the proper model run.

!    Calculate h,u,v with these tendencies
    h = h + dt*dhdtold
    u = u + dt*dudtold
    v = v + dt*dvdtold

!    Apply the boundary conditions
!    Enforce no normal flow boundary condition
!    and no flow in dry cells.
!    no/free-slip is done inside dudt and dvdt subroutines.
!    hfacW and hfacS are zero where the transition between
!    wet and dry cells occurs. wetmask is 1 in wet cells,
!    and zero in dry cells.
    do k = 1,layers
        u(:,:,k) = u(:,:,k)*hfacW*wetmask(1:nx,:)
        v(:,:,k) = v(:,:,k)*hfacS*wetmask(:,1:ny)
    enddo


!    Now the model is ready to start.
!    We have h, u, v at the zeroth time step, and the tendencies at two older time steps.
!    The model then solves for the
!    tendencies at the current step before solving for the fields at the 
!    next time step.


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!        MAIN LOOP OF THE MODEL STARTS HERE        !!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    do 9999 n=1,nTimeSteps
      
!   time varying winds
    if (UseSinusoidWind .eqv. .TRUE.) then

        wind_x = base_wind_x*(wind_alpha +  &
            wind_beta*sin(((2d0*pi*n*dt)/wind_period) - wind_t_offset))

        wind_y = base_wind_y*(wind_alpha +  &
            wind_beta*sin(((2d0*pi*n*dt)/wind_period) - wind_t_offset))

    else if (UseStochWind .eqv. .TRUE.) then
        
        if (mod(n-1,n_stoch_wind).eq.0) then 

!           gives a pseudorandom number in range 0 <= x < 1
            call random_number(stoch_wind_mag)
!           convert to -1 <= x < 1
            stoch_wind_mag = (stoch_wind_mag - 0.5d0)*2d0

            wind_x = base_wind_x*(wind_alpha +  &
                wind_beta*stoch_wind_mag)

            wind_y = base_wind_y*(wind_alpha +  &
                wind_beta*stoch_wind_mag)
        endif
    endif



!     calculate Bernoulli potential
    call evaluate_b(b,h,u,v,nx,ny,layers,gr)
!
!    calculate relative vorticity
    call evaluate_zeta(zeta,u,v,nx,ny,layers,dx,dy)
!
!     Calculate dhdt, dudt, dvdt at current time step
    call evaluate_dhdt(dhdt, h,u,v,ah,dx,dy,nx,ny,layers, &
        spongeHTimeScale,spongeH,wetmask)

    call evaluate_dudt(dudt, h,u,v,b,zeta,wind_x,fu, au,ar,slip,&
        dx,dy,hfacN,hfacS,nx,ny,layers,rho0,&
        spongeUTimeScale,spongeU)

    call evaluate_dvdt(dvdt, h,u,v,b,zeta,wind_y,fv, au,ar,slip, &
        dx,dy,hfacW,hfacE,nx,ny,layers,rho0,spongeVTimeScale,spongeV)

!    Use dh/dt, du/dt and dv/dt to step h, u and v forward in time with
!    Adams-Bashforth third order linear multistep method


    unew=u+dt*(23d0*dudt - 16d0*dudtold + 5d0*dudtveryold)/12d0

    vnew=v+dt*(23d0*dvdt - 16d0*dvdtold + 5d0*dvdtveryold)/12d0

    hnew=h+dt*(23d0*dhdt - 16d0*dhdtold + 5d0*dhdtveryold)/12d0

 
!    Enforce no normal flow boundary condition
!    and no flow in dry cells.
!    no/free-slip is done inside dudt and dvdt subroutines.
!    hfacW and hfacS are zero where the transition between
!    wet and dry cells occurs. wetmask is 1 in wet cells,
!    and zero in dry cells.
    do k = 1,layers
        unew(:,:,k) = unew(:,:,k)*hfacW*wetmask(1:nx,:)
        vnew(:,:,k) = vnew(:,:,k)*hfacS*wetmask(:,1:ny)
    enddo
    

    
! code to stop layers getting too thin
    counter = 0
    
    do k = 1,layers
      do j=1,ny-1
        do i=1,nx-1    
          if (hnew(i,j,k) .lt. hmin) then
        hnew(i,j,k) = hmin
        counter = counter + 1
        if (counter .eq. 1) then
          ! write a file saying that the layer thickness value dropped below hmin and this line has been used
          OPEN(UNIT=10, FILE='layer thickness dropped below hmin.txt', ACTION="write", STATUS="unknown", &
          FORM="formatted", POSITION = "append")
          write(10,1111) n
1111              format( "layer thickness dropped below hmin at time step ", 1i10.10)  
          CLOSE(UNIT=10)
        endif
          endif
        end do
      end do
    end do

!    Accumulate average fields
    hav = hav + hnew
    uav = uav + unew
    vav = vav + vnew

!     shuffle arrays: old -> very old,  present -> old, new -> present
!    height and velocity fields
    h = hnew
    u = unew
    v = vnew

!    tendencies (old -> very old)
    dhdtveryold = dhdtold
    dudtveryold = dudtold
    dvdtveryold = dvdtold

!    tendencies (current -> old)
    dhdtold = dhdt
    dudtold = dudt
    dvdtold = dvdt

!     now have new fields in main arrays and old fields in old arrays

!--------------------------- Code for generating output -------------------------------------
!     write data to file? 
      if (mod(n-1,nwrite).eq.0) then 

        !     
        !----------------------- Snapshot Output -----------------
        write(num,'(i10.10)') n

        !output the data to a file

        open(unit = 10, status='replace',file='output/snap.h.'//num,form='unformatted')  
        write(10) h
        close(10) 
        open(unit = 10, status='replace',file='output/snap.u.'//num,form='unformatted')  
        write(10) u
        close(10) 
        open(unit = 10, status='replace',file='output/snap.v.'//num,form='unformatted')  
        write(10) v
        close(10) 

        if (DumpWind .eqv. .TRUE.) then
            open(unit = 10, status='replace',file='output/wind_x.'//num,form='unformatted')  
            write(10) wind_x
            close(10) 
            open(unit = 10, status='replace',file='output/wind_y.'//num,form='unformatted')  
            write(10) wind_y
            close(10)
        endif 

        ! Check if there are NaNs in the data
        call break_if_NaN(h,nx,ny,layers,n)
        !     call break_if_NaN(u,nx,ny,layers,n)
        !     call break_if_NaN(v,nx,ny,layers,n)     
    
    endif

        !----------------------- Average Output -----------------
      if (avwrite .eq. 0) then 
        go to 120
      else if (mod(n-1,avwrite).eq.0) then 

        hav=hav/real(avwrite)
        uav=uav/real(avwrite)
        vav=vav/real(avwrite)

        write(num,'(i10.10)') n

        !output the data to a file

        open(unit = 10, status='replace',file='output/av.h.'//num,form='unformatted')  
        write(10) hav
        close(10) 
        open(unit = 10, status='replace',file='output/av.u.'//num,form='unformatted')  
        write(10) uav
        close(10) 
        open(unit = 10, status='replace',file='output/av.v.'//num,form='unformatted')  
        write(10) vav
        close(10) 

        ! Check if there are NaNs in the data
        call break_if_NaN(h,nx,ny,layers,n)
        !     call break_if_NaN(u,nx,ny,layers,n)
        !     call break_if_NaN(v,nx,ny,layers,n)     

        !    Reset average quantities
        hav = 0.0
        uav = 0.0
        vav = 0.0
        !       h2av=0.0
    
120 endif

9999  continue

    OPEN(UNIT=10, FILE='run_finished.txt', ACTION="write", STATUS="unknown", &
    FORM="formatted", POSITION = "append")
    write(10,1112) n
    1112      format( "run finished at time step ", 1i10.10)  
    CLOSE(UNIT=10)

    stop

    END PROGRAM reduced_gravity



! ----------------------------- Beginning of the subroutines --------------------
!
!-----------------------------------------------------------------------------------
!> Evaluate the Bornoulli Potential for each grid box.
!! B is evaluated at the tracer point, centre of the grid, for each grid box.
!!
!! \f[
!!    B = \frac{u \cdot u}{2} + g_{r}h
!!  \f]
!! 
!!!
    subroutine evaluate_b(b,h,u,v,nx,ny,layers,gr)
!    Evaluate Bernoulli Potential at centre of grid box
    integer nx,ny,layers
    integer i,j,k
    double precision h(0:nx,0:ny,layers),u(nx,0:ny,layers),v(0:nx,ny,layers)
    double precision b(nx,ny,layers), gr(layers)
    double precision h_temp, b_proto

    b = 0d0

    do k = 1,layers !move through the different layers of the model
      do j=1,ny-1 !move through longitude
        do i=1,nx-1 ! move through latitude
          ! The following loops are to get the pressure term in the Bernoulli Potential
          b_proto = 0d0
          do l = k,layers
            h_temp = 0d0
            do m = 1,l
              h_temp = h_temp + h(i,j,m) !sum up the layer thicknesses
            end do
            b_proto = b_proto + gr(l)*h_temp !sum up the product of reduced gravity and summed layer thicknesses to form the pressure componenet of the Bernoulli Potential term
          end do
          b(i,j,k)= b_proto + (u(i,j,k)**2+u(i+1,j,k)**2+v(i,j,k)**2+v(i,j+1,k)**2)/4.0d0
          ! Add the (u^2 + v^2)/2 term to the pressure componenet of the Bernoulli Potential
        end do 
      end do 
    end do


    return
    end subroutine
!---------------------------------------------------------------------------------------
!>    Evaluate relative vorticity at lower left grid boundary (du/dy 
!!    and dv/dx are at lower left corner as well)
    subroutine evaluate_zeta(zeta,u,v,nx,ny,layers,dx,dy)
    integer nx,ny,layers
    integer i,j,k
    double precision h(0:nx,0:ny,layers),u(nx,0:ny,layers),v(0:nx,ny,layers)
    double precision zeta(nx,ny,layers)
    double precision dx, dy

    zeta = 0d0
    
    do k = 1,layers
      do j=1,ny
        do i=1,nx
          zeta(i,j,k)=(v(i,j,k)-v(i-1,j,k))/dx-(u(i,j,k)-u(i,j-1,k))/dy
        end do 
      end do 
    end do

    return
    end subroutine
!------------------------------------------------------------------------------------------
!> Calculate the tendency of layer thickness for each of the active layers
!! dh/dt is in the centre of each grid point.
    subroutine evaluate_dhdt(dhdt, h,u,v,ah,dx,dy,nx,ny,layers, spongeTimeScale,spongeH,wetmask)
!    dhdt is evaluated at the centre of the grid box
    integer nx,ny,layers
    integer i,j,k
    double precision dhdt(0:nx,0:ny,layers), h(0:nx,0:ny,layers)
    double precision u(nx,0:ny,layers),v(0:nx,ny,layers)
    double precision spongeTimeScale(0:nx,0:ny,layers)
    double precision spongeH(0:nx,0:ny,layers)
    double precision wetmask(0:nx,0:ny)
    double precision dx, dy
    double precision ah(layers)

    dhdt = 0d0

    do k = 1,layers
      do j=1,ny-1
        do i=1,nx-1
          dhdt(i,j,k)=ah(k)*(h(i+1,j,k)*wetmask(i+1,j) + &
                            (1d0 - wetmask(i+1,j))*h(i,j,k) & ! boundary
                        + h(i-1,j,k)*wetmask(i-1,j) + &
                            (1d0 - wetmask(i-1,j))*h(i,j,k) & ! boundary
                        - 2*h(i,j,k))/ &
        (dx*dx) + & ! x-componenet 
          ah(k)*(h(i,j+1,k)*wetmask(i,j+1) + &
                (1d0 - wetmask(i,j+1))*h(i,j,k) & ! reflect value around boundary
            + h(i,j-1,k)*wetmask(i,j-1) + &
                (1d0 - wetmask(i,j-1))*h(i,j,k) & ! reflect value around boundary
            - 2*h(i,j,k))/ &
        (dy*dy) - & !y-component horizontal diffusion
        ((h(i,j,k)+h(i+1,j,k))*u(i+1,j,k) - (h(i-1,j,k)+h(i,j,k))*u(i,j,k))/(dx*2d0) - & !d(hu)/dx
        ((h(i,j,k)+h(i,j+1,k))*v(i,j+1,k) - (h(i,j-1,k)+h(i,j,k))*v(i,j,k))/(dy*2d0) + & !d(hv)/dy
        spongeTimeScale(i,j,k)*(spongeH(i,j,k)-h(i,j,k)) ! forced relaxtion in the sponge regions.
        end do 
      end do 
    end do

    do k = 1,layers
        dhdt(:,:,k) = dhdt(:,:,k)*wetmask
    end do

    return
    end subroutine
!--------------------------------------------------------------------------------------------
!> Calculate the tendency of zonal velocity for each of the active layers

    subroutine evaluate_dudt(dudt, h,u,v,b,zeta,wind_x,fu, au,ar,slip,dx,dy,hfacN,hfacS,nx,ny,layers,rho0,spongeTimeScale,spongeU)
!    dudt(i,j) is evaluated at the centre of the left edge of the grid box,
!     the same place as u(i,j)
    integer nx,ny,layers
    integer i,j,k
    double precision dudt(nx,0:ny,layers), h(0:nx,0:ny,layers)
    double precision u(nx,0:ny,layers),v(0:nx,ny,layers)
    double precision fu(nx,0:ny), zeta(nx,ny,layers)
    double precision b(nx,ny,layers)
    double precision spongeTimeScale(nx,0:ny,layers)
    double precision spongeU(nx,0:ny,layers)
    double precision wind_x(nx, 0:ny)
    double precision dx, dy
    double precision au, ar, rho0, slip
    double precision hfacN(0:nx,ny)
    double precision hfacS(0:nx,ny)

    dudt = 0d0

    do k = 1,layers
      do i=2,nx-1  
        do j=1,ny-1
            dudt(i,j,k)= au*(u(i+1,j,k)+u(i-1,j,k)-2.0d0*u(i,j,k))/ & 
            (dx*dx)  + &   !    x-component
            au*(u(i,j+1,k)+u(i,j-1,k)-2.0d0*u(i,j,k)+ &
            ! boundary conditions
            (1.0d0 - 2.0d0*slip)*(1.0d0 - hfacN(i,j))*u(i,j,k) + &
            (1.0d0 - 2.0d0*slip)*(1.0d0 - hfacS(i,j))*u(i,j,k))/ &
            (dy*dy) + & !y-component
    !    together make the horizontal diffusion term
            0.25d0*(fu(i,j)+0.5d0*(zeta(i,j,k)+zeta(i,j+1,k)))* &
            (v(i-1,j,k)+v(i,j,k)+v(i-1,j+1,k)+v(i,j+1,k)) - & !vorticity term
            (b(i,j,k) - b(i-1,j,k))/dx + & !Bernoulli Potential term
            spongeTimeScale(i,j,k)*(spongeU(i,j,k)-u(i,j,k)) ! forced relaxtion in the sponge regions)
            if (k .eq. 1) then ! only have wind forcing on the top layer
                !This will need refining in the event of allowing outcropping.
                dudt(i,j,k) = dudt(i,j,k) + wind_x(i,j)/(rho0*h(i,j,k)) !wind forcing
            endif
            if (layers .gt. 1) then ! only evaluate vertical momentum diffusivity if more than 1 layer
                if (k .eq. 1) then ! adapt vertical momentum diffusivity for 2+ layer model -> top layer
                    dudt(i,j,k) = dudt(i,j,k) - 1.0d0*ar*(u(i,j,k) - 1.0d0*u(i,j,k+1))
                else if (k .eq. layers) then ! bottom layer
                    dudt(i,j,k) = dudt(i,j,k) - 1.0d0*ar*(u(i,j,k) - 1.0d0*u(i,j,k-1))
                else ! mid layer/s
                    dudt(i,j,k) = dudt(i,j,k) - 1.0d0*ar*(2.0d0*u(i,j,k) - 1.0d0*u(i,j,k-1) - 1.0d0*u(i,j,k+1))
                endif
            endif
        end do
      end do
    end do
    return
    end subroutine
!------------------------------------------------------------------------------------
!> Calculate the tendency of meridional velocity for each of the active layers

    subroutine evaluate_dvdt(dvdt, h,u,v,b,zeta,wind_y,fv, au,ar,slip,dx,dy,hfacW,hfacE,nx,ny,layers,rho0,spongeTimeScale,spongeV)
!    dvdt(i,j) is evaluated at the centre of the bottom edge of the grid box,
!     the same place as v(i,j)
    integer nx,ny,layers
    integer i,j,k
    double precision dvdt(0:nx,ny,layers), h(0:nx,0:ny,layers),u(nx,0:ny,layers),v(0:nx,ny,layers)
    double precision fv(0:nx,ny), zeta(nx,ny,layers), b(nx,ny,layers)
    double precision spongeTimeScale(0:nx,ny,layers)
    double precision spongeV(0:nx,ny,layers)
    double precision wind_y(0:nx, ny)
    double precision dx, dy
    double precision au, ar, slip
    double precision rho0
    double precision hfacW(nx,0:ny)
    double precision hfacE(nx,0:ny)

    dvdt = 0d0
    
    do k = 1,layers
      do j=2,ny-1
        do i=1,nx-1
            dvdt(i,j,k)=au*(v(i+1,j,k)+v(i-1,j,k)-2.0d0*v(i,j,k) + & 
            ! boundary conditions
            (1.0d0 - 2.0d0*slip)*(1.0d0 - hfacW(i,j))*v(i,j,k) + &
            (1.0d0 - 2.0d0*slip)*(1.0d0 - hfacE(i,j))*v(i,j,k))/ &
            (dx*dx) + & !x-component
              au*(v(i,j+1,k) + v(i,j-1,k) - 2.0d0*v(i,j,k))/ &
            (dy*dy) - & 
    !    y-component. Together these make the horizontal diffusion term
            0.25d0*(fv(i,j)+0.5d0*(zeta(i,j,k)+zeta(i+1,j,k)))* &
            (u(i,j-1,k)+u(i,j,k)+u(i+1,j-1,k)+u(i+1,j,k)) - & !vorticity term
            (b(i,j,k)-b(i,j-1,k))/dy + & ! Bernoulli Potential term
            spongeTimeScale(i,j,k)*(spongeV(i,j,k)-v(i,j,k)) ! forced relaxtion to vsponge (in the sponge regions)
            if (k .eq. 1) then ! only have wind forcing on the top layer
                !This will need refining in the event of allowing outcropping.
                dvdt(i,j,k) = dvdt(i,j,k) + wind_y(i,j)/(rho0*h(i,j,k)) !wind forcing
            endif
            if (layers .gt. 1) then ! only evaluate vertical momentum diffusivity if more than 1 layer
                if (k .eq. 1) then ! adapt vertical momentum diffusivity for 2+ layer model -> top layer
                    dvdt(i,j,k) = dvdt(i,j,k) - 1.0d0*ar*(v(i,j,k) - 1.0d0*v(i,j,k+1))
                else if (k .eq. layers) then ! bottom layer
                    dvdt(i,j,k) = dvdt(i,j,k) - 1.0d0*ar*(v(i,j,k) - 1.0d0*v(i,j,k-1))
                else ! mid layer/s
                    dvdt(i,j,k) = dvdt(i,j,k) - 1.0d0*ar*(2.0d0*v(i,j,k) - 1.0d0*v(i,j,k-1) - 1.0d0*v(i,j,k+1))
                endif
            endif
        end do 
      end do 
    end do

    return
    end subroutine
!--------------------------------------------------------------------------------------
!> Export data to file
    subroutine write_data(data,nx,ny,filename)

!    A subroutine to output data as a 2D array for plotting and things

    implicit none

    integer nx,ny
    double precision data(0:nx,0:ny)
    character (len=*) filename
    integer i,j

!    to show which field is being output
!    print *,filename 

    OPEN(UNIT=13, FILE=filename, ACTION="write", STATUS="replace", &
        FORM="formatted")

    do j=1,ny-1
        write(13,1000) (data(i,j),i=1,nx-1)
1000         format( 400f32.16)  
    end do  
    CLOSE(UNIT=13)
    return
    end
!------------------------------------------------------------------------------------
!> Check to see if there are any NaNs in the data field and stop the calculation
!! if any are found.
    subroutine break_if_NaN(data,nx,ny,layers,n)

!     To stop the program if it detects at NaN in the variable being checked

    integer nx, ny,layers,n
    double precision data(0:nx, 0:ny,layers)


      do k=1,layers
      do j=1,ny-1
      do i=1,nx-1
        if (data(i,j,k).ne.data(i,j,k))  then
        ! write a file saying so
        OPEN(UNIT=10, FILE='NaN detected.txt', ACTION="write", STATUS="replace", &
        FORM="formatted")
        write(10,1000) n
1000         format( "NaN detected at time step ", 1i10.10)  
        CLOSE(UNIT=10)
        ! print it on the screen
        print *, 'NaN detected' 
        ! Stop the code
        STOP
        endif
      end do
      end do
      end do
    
    return
    end

!-----------------------------------------------------------------------------------
!> Define masks for boundary conditions in u and v
!! this finds locations where neighbouring grid boxes are not the same (i.e. one is land and one is ocean). 
!! in the output
!! 0 means barrier
!! 1 mean open

    subroutine calc_boundary_masks(wetmask,hfacW,hfacE,hfacS,hfacN,nx,ny)

    implicit none
    integer nx, ny
    double precision wetmask(0:nx,0:ny)
    double precision temp(0:nx,0:ny)
    double precision hfacW(nx,0:ny)
    double precision hfacE(nx,0:ny)
    double precision hfacN(0:nx,ny)
    double precision hfacS(0:nx,ny)
    integer i,j


    hfacW = 1d0
! and now for all  western cells
    hfacW(1,:) = 0d0
    
    temp = 0.0
    do j = 0,ny
      do i = 1,nx
        temp(i,j) = wetmask(i-1,j)- wetmask(i,j)
      enddo
    enddo
        
    do j = 0,ny
      do i = 1,nx
        if (temp(i,j) .ne. 0.0) then
          hfacW(i,j) = 0d0
        endif
      enddo
    enddo

    hfacE = 1
! and now for all  eastern cells
    hfacE(nx,:) = 0
    
    temp = 0.0
    do j = 0,ny
      do i = 1,nx-1
        temp(i,j) = wetmask(i,j)- wetmask(i+1,j)
      enddo
    enddo
        
    do j = 0,ny
      do i = 1,nx
        if (temp(i,j) .ne. 0.0) then
          hfacE(i,j) = 0d0
        endif
      enddo
    enddo


    hfacS = 1
!   all southern cells
    hfacS(:,1) = 0
    temp = 0.0
    do j = 1,ny
      do i = 0,nx
        temp(i,j) = wetmask(i,j-1)- wetmask(i,j)
      enddo
    enddo
    
    do j = 1,ny
      do i = 0,nx
        if (temp(i,j) .ne. 0.0) then
          hfacS(i,j) = 0d0
        endif
      enddo
    enddo

    hfacN = 1
!   all northern cells
    hfacN(:,ny) = 0
    temp = 0.0
    do j = 1,ny-1
      do i = 0,nx
        temp(i,j) = wetmask(i,j)- wetmask(i,j+1)
      enddo
    enddo
    
    do j = 1,ny
      do i = 0,nx
        if (temp(i,j) .ne. 0.0) then
          hfacN(i,j) = 0d0
        endif
      enddo
    enddo

    return
    end

!-----------------------------------------------------------------


    subroutine read_input_fileH(name,array,default,nx,ny,layers)

    implicit none
    character(30) name
    integer nx, ny, layers, k
    double precision array(0:nx,0:ny,layers), default(layers)

    if (name.ne.'') then
        open(unit = 10, form='unformatted', file=name)  
        read(10) array
        close(10) 
    else
      do k = 1,layers
        array(:,:,k) = default(k)
      enddo
    endif

    return
    end


    subroutine read_input_fileH_2D(name,array,default,nx,ny)

    implicit none
    character(30) name
    integer nx, ny
    double precision array(0:nx,0:ny), default

    if (name.ne.'') then
        open(unit = 10, form='unformatted', file=name)  
        read(10) array
        close(10) 
    else
        array = default
    endif

    return
    end



    subroutine read_input_fileU(name,array,default,nx,ny,layers)

    implicit none
    character(30) name
    integer nx, ny,layers
    double precision array(nx,0:ny,layers), default

    if (name.ne.'') then
        open(unit = 10, form='unformatted', file=name)  
        read(10) array
        close(10) 
    else
        array = default
    endif

    return
    end


    subroutine read_input_fileV(name,array,default,nx,ny,layers)

    implicit none
    character(30) name
    integer nx, ny, layers
    double precision array(0:nx,ny,layers), default

    if (name.ne.'') then
        open(unit = 10, form='unformatted', file=name)  
        read(10) array
        close(10) 
    else
        array = default
    endif

    return
    end


!-----------------------------------------------------------------
!> Robustly produce a different random seed for each run.
!! Code adapted from http://web.ph.surrey.ac.uk/fortweb/glossary/random_seed.html


    subroutine ranseed()

    implicit none

    ! ----- variables for portable seed setting -----
    INTEGER :: i_seed
    INTEGER, DIMENSION(:), ALLOCATABLE :: a_seed
    INTEGER, DIMENSION(1:8) :: dt_seed
    ! ----- end of variables for seed setting -----

    ! ----- Set up random seed portably -----
    CALL RANDOM_SEED(size=i_seed)
    ALLOCATE(a_seed(1:i_seed))
    CALL RANDOM_SEED(get=a_seed)
    CALL DATE_AND_TIME(values=dt_seed)
    a_seed(i_seed)=dt_seed(8); a_seed(1)=dt_seed(8)*dt_seed(7)*dt_seed(6)
    call random_seed(put=a_seed)
    return
    end

